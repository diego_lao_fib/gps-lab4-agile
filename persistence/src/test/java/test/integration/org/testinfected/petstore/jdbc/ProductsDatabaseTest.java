package test.integration.org.testinfected.petstore.jdbc;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.testinfected.petstore.db.Access.idOf;
import static org.testinfected.petstore.db.Access.productOf;
import static test.support.org.testinfected.petstore.builders.ItemBuilder.a;
import static test.support.org.testinfected.petstore.builders.ProductBuilder.aProduct;
import static test.support.org.testinfected.petstore.jdbc.HasFieldWithValue.hasField;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Test;
import org.testinfected.petstore.db.ItemsDatabase;
import org.testinfected.petstore.db.JDBCTransactor;
import org.testinfected.petstore.db.ProductsDatabase;
import org.testinfected.petstore.product.DuplicateItemException;
import org.testinfected.petstore.product.DuplicateProductException;
import org.testinfected.petstore.product.Item;
import org.testinfected.petstore.product.ItemNumber;
import org.testinfected.petstore.product.Product;
import org.testinfected.petstore.product.ProductCatalog;
import org.testinfected.petstore.transaction.QueryUnitOfWork;
import org.testinfected.petstore.transaction.Transactor;

import test.support.org.testinfected.petstore.builders.Builder;
import test.support.org.testinfected.petstore.builders.ItemBuilder;
import test.support.org.testinfected.petstore.builders.ProductBuilder;
import test.support.org.testinfected.petstore.jdbc.Database;

public class ProductsDatabaseTest {
	Database database = Database.test();
    Connection connection = database.connect();
    Transactor transactor = new JDBCTransactor(connection);
    ProductsDatabase productCatalog = new ProductsDatabase(connection);

    @After public void
    closeConnection() throws SQLException {
        connection.close();
    }
    
    @Test public void findsProductByNumber() throws Exception {
        Product dog = savedProductFrom(aProduct("12345678"));
        

        Product found = productCatalog.findByNumber("12345678");
        assertThat("matched item", found.getNumber().equals("12345678"));
    }
    
    @Test public void findsProductByName() throws Exception {
        savedProductFrom(aProduct().named("Patatas"));

        List <Product> found = productCatalog.findByKeyword("Patatas");
        Product patata = search(found, "Patatas");
        assertThat("matched item", patata.getName().equals("Patatas"));
    }
    
    
    
    @Test public void findsProductByDesc() throws Exception {
        savedProductFrom(aProduct().describedAs("Patatas"));

        List <Product> found = productCatalog.findByKeyword("Patatas");
        Product patata = searchdesc(found, "Patatas");
        assertThat("matched item", patata.getDescription().equals("Patatas"));
    }
    
    @Test
    public void restoreFullProduct() throws Exception{
    	Product p = aProduct().withNumber("444").build();
    	productCatalog.add(p);
        Product patata = productCatalog.findByNumber("444");
    	assertThat("validation all data is restored on Product retrieved", p, sameProductAs(patata));
    }
    
    @Test(expected=DuplicateProductException.class)
    public void uniqueProductId() throws Exception{
    	ProductBuilder existingProduct = aProduct().withNumber("12345");
        given(existingProduct);
        Product prod = savedProductFrom(existingProduct);
        throw new DuplicateProductException(prod);
    }
    
    @Test
    public void errorOnProductNotExistsInDB(){
    	Product prod = productCatalog.findByNumber("52843276");
    	assertThat("validation on product not exists", prod==null);
    }
    
    private static Product search(List<Product> A, String B) {
    	Iterator <Product> it = A.iterator();
    	while (it.hasNext()) {
    		Product p = it.next();
    		if (B.equals(p.getName())) return p;  
    	}
    	
    	Product p2 = aProduct().build();
		return p2;
    }
    
    
    private static Product searchdesc(List<Product> A, String B) {
    	Iterator <Product> it = A.iterator();
    	while (it.hasNext()) {
    		Product p = it.next();
    		if (B.equals(p.getDescription())) return p;  
    	}
    	
    	Product p2 = aProduct().build();
		return p2;
    }

    
    private Matcher<Product> sameProductAs(Product original) {
        return allOf(hasField("id", equalTo(idOf(original).get())),
                samePropertyValuesAs(original));
    }

    private Product savedProductFrom(final Builder<Product> builder) throws Exception {
        return transactor.performQuery(new QueryUnitOfWork<Product>() {
            public Product query() throws Exception {
                Product product = builder.build();
                productCatalog.add(product);
                return product;
            }
        });
    }

    @SafeVarargs
    private final void given(final Builder<Product>... products) throws Exception {
        for (final Builder<Product> product : products) {
            savedProductFrom(product);
        }
    }
}
