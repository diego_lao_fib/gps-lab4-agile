package test.unit.org.testinfected.petstore.order;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;
import static test.support.org.testinfected.petstore.builders.OrderBuilder.anOrder;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.on;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.violates;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.withMessage;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import org.junit.Test;
import org.testinfected.petstore.order.OrderNumber;

public class OrderNumberTest {

	@Test 
	public void IDReferenceDiferentNumber() {
		assertThat("validation of no repetition of id",
				!anOrder().withNumber("13").equals(anOrder().withNumber("14")));
	}

	@Test
	public void IDReferenceSameNumber() {
		assertThat("validation of no repetition of id",
				!anOrder().withNumber("13").equals(anOrder().withNumber("13")));
	}
	
	@Test
	public void NormalitzationNumber1Digit() {
	
		OrderNumber x = new OrderNumber(1);
		
		assertThat("validation of normalitzation order number",
				anOrder().with(x).build().getNumber().equals("00000001"));
		
	}
	
	
	@Test
	public void NormalitzationNumber2Digits() {
	
		OrderNumber x = new OrderNumber(13);
		
		assertThat("validation of normalitzation order number",
				anOrder().with(x).build().getNumber().equals("00000013"));
		
	}
}
