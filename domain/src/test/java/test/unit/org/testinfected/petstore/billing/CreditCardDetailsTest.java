package test.unit.org.testinfected.petstore.billing;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.builders.AddressBuilder.anAddress;
import static test.support.org.testinfected.petstore.builders.CreditCardBuilder.aCreditCard;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.on;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.violates;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.withMessage;

import org.junit.Test;
import org.testinfected.petstore.billing.CreditCardType;

import test.support.org.testinfected.petstore.builders.AddressBuilder;
import test.support.org.testinfected.petstore.builders.CreditCardBuilder;

public class CreditCardDetailsTest {

	String MISSING = null;

	/* Una targeta invàlida que no tingui tota la numeració */
	@Test
	public void isInvalidWithoutAllNumbers() {
		assertThat("validation of credit card details with missing some number",
				validationOf(aCreditCard().withNumber("43210")), violates(on("cardNumber"), withMessage("incorrect")));
	}

	/* Una targeta amb la una data de caducitat incorrecte */
	@Test
	public void isInvalidWithoutCardExpiryDate() {
		assertThat("validation of credit card details with missing expiry date",
				validationOf(aCreditCard().withExpiryDate(MISSING)),
				violates(on("cardExpiryDate"), withMessage("missing")));
	}

	/* Una targeta que li falti el nom del titular */
	@Test
	public void isInvalidWithoutAFirstName() {
		assertThat("validation of credit card details with missing first name of billingAddress",
				validationOf(aCreditCard().billedTo(anAddress().withFirstName(MISSING))),
				violates(on("billingAddress.firstName"), withMessage("missing")));
	}

	/* Una targeta amb tots els camps correctes */
	@Test
	public void isValidWithAFullNameAndCorrectCardNumber() {
		AddressBuilder validAddress = anAddress().withFirstName("Joe").withLastName("Blow");
		CreditCardBuilder card = aCreditCard().billedTo(validAddress).withExpiryDate("12/26")
				.ofType(CreditCardType.visa).withNumber("4111111111111111");
		assertThat("validation of valid address", validationOf(card), succeeds());
	}
}
