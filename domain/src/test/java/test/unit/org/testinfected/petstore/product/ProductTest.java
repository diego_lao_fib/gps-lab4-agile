package test.unit.org.testinfected.petstore.product;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static test.support.org.testinfected.petstore.builders.ProductBuilder.aProduct;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;


import org.junit.Test;
import org.testinfected.petstore.product.Product;
public class ProductTest {


    @Test
    public void hasDefaultPhoto() {
        assertThat("Product has the default photo", 
        		validationOf(aProduct().withoutAPhoto()),
        		succeeds());
    }
    
    @Test
    public void IDReferenceNumber() {
    	Product product1 = aProduct().withNumber("1234").build();
    	Product product2 = aProduct().withNumber("1237").build();
    	Product product3 = aProduct().withNumber("1234").build();
        assertFalse("Two products with different number is not the same product", product1.equals(product2));
        assertTrue("Two products with the same number is the same product", product1.equals(product3));
    }
}
